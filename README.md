このファイルは，Yu-Cheng Tingのネットワークシミュレーション用ソースコードである．
このソースコードは，車両ネットワークシミュレーションフレームワークであるVeins 4.7.1を用いて作ったものである．
VeinsはネットワークシミュレータのOMNet++と交通流シミュレータのSUMOに基づいている．
OMNet++は https://omnetpp.org/ から無料でダウンロードできる．
SUMOは http://sumo.sourceforge.net/ から無料でダウンロードできる．
Veinsについては，https://veins.car2x.org/ で参考できる．無料のダウンロードも可能である．
VeinsのファイルをOMnet++に導入すれば，実行することが可能である．
OMNet++の使い方については，公式マニュアルを参考してください．
OMNet++初めて使いの方は，OMNet++ TicTocの練習が必要である．

OMNet++とSUMOのインストール方法については，それぞれの公式ページを参考すること．

Veinsを実行する手順について
1. まず，フォルダ名のYu-Cheng Tingをほかの英字名に変更すること(スペースなしで)．
   同じフォルダで，コマンドファイルを作り，実行する．　入力するコマンドは　D:\Network Simulation\veins\sumo-launchd.py" -vv -c "D:\Program Files\DLR\Sumo\bin\sumo.exe
   パスやフォルダ名個人によって変わる．これはSUMOとOMNet++を連携するための動作である．
2. フォルダ"example"の中にあるberkeley01-1を実行すれば，シミュレーションすることが可能である．公式ページからダウンロードの場合が，erlangenというファイルを実行する．
