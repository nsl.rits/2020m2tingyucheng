//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/*創建車輛行為的起點檔案*/
/**
 * TraCIMobility* mobility;宣告在BaseWaveApplLayer.h
 * int myId宣告在BaseWaveApplLayer.h
 * Coord curPosition;宣告在BaseWaveApplLayer.h
 * getCurrentPosition()宣告在BaseMobility.h
 * TraCICommandInterface* traci;被宣告在TraCICommandInterface.h
 * manager = TraCIScenarioManagerAccess().get();的類別定義在TraCIScenarioManger.h
 * */


#include "MyVeinsApp.h"
#include "veins/modules/messages/WaveShortMessage_m.h"
#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"

using namespace std;
using namespace Veins;
//using Veins::TraCICommandInterface;

Define_Module(MyVeinsApp);

/*無法在標頭檔宣告的變數*/
Coord positionOftarget;
Coord positionOftarget01;
Coord curPositionOftracker;
double distanceCalculate = 0;
double maxSpeed00;
int forward_wait_time = 3;
simtime_t time_to_assign;
simtime_t time_to_report;
simtime_t time_to_send_lost_message;
simtime_t temp_t;
string nodeString;
setup* my_setup = new setup;
cModule* xModule;
int count_of_report = 0;
int count_of_rsu_reply = 0;
int report_index[50] = {0};
int save_temp_3 = 10;
int wsm_version = 0;
int handoff_times = 0;
int rsu_reply = 0;
/*無法在標頭檔宣告的變數*/

void MyVeinsApp::initialize(int stage) {
    BaseWaveApplLayer::initialize(stage);

    if (stage == 0) {
        /*Initializing members and pointers of your application goes here*/
        /*初始化應用程序的成員和pointer就在這裡*/

        //manager = TraCIScenarioManagerAccess().get();   /*TraCIScreenRecoder.cc有類似定義*/
        //TraCICommandInterface* traciInterface = manager->getCommandInterface(); /*重要*/
        //TraCIMobilityAccess().get(prTargetCarModule);    /*嘗試取得主機*/

        //traciInterface->getDistance(position1, position2, returnDrivingDistance)
        // TraCIConnection* myconnecton;

        // TraCICommandInterface obj000(myconnection);
        // TraCICommandInterface obj005;
        // obj003.getDistance(position1, position2, returnDrivingDistance)

        //distanceCalculate = TraCICommandInterface::getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = TraCICommandInterface::getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = obj000.getDistance(curPosition, positionOftarget, 1);
        // TraCICommandInterface::getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = MIXIM_API::distance(positionOftarget);

        EV << "Initializing " << par("appName").stringValue() << std::endl;

        my_setup->setRequest_count(0);
        my_setup->setF_count_of_handoffmessage(0);
        my_setup->setF_count_of_report(0);

        handoff_statistics.setName("handoff_statistics");
        tracking_bool.setName("tracking_now_or_not");

        if (getParentModule()->getIndex() == 100000) /*myId的宣告藏在BaseWaveApplLayer.h*/ /*the first Tracker*/
        {
            //findHost()->getDisplayString().updateWith("r=16,red");    /*可以改變車的顏色*/
            /*populateWSM()簡要設置WSM，BSM或WSA中的所有必要字段，呼叫BaseWaveApplLay.cc的函數*/
            /*參考 https://github.com/sommer/veins/blob/master/src/veins/modules/application/ieee80211p/BaseWaveApplLayer.h */

            am_i_a_tracker = 1;
            my_setup->insertTrackers_index(getParentModule()->getIndex());
            curPositionOftracker = TraCIMobilityAccess().get(getParentModule())->getCurrentPosition(); /*位置取得語法測試*/
        }

        if (getParentModule()->getIndex() == target_index)    /*暫定：初始目標車輛，getParentModule()->getId()等同myId*/
        {
            //findHost()->getDisplayString().updateWith("r=16,red"); /*目標標記紅色*/
            //xModule = manager->getManagedModule("22");   /*return hosts[nodeId]語法錯誤無法使用*/
            //manager->getManagedHosts();                  /*取得所有主機，未測試語法*/
            //TraCIMobilityAccess().get(getParentModule()->getParentModule())->getDisplayString().updateWith("r=16,red");
            /*TraCIMobilityAccess().get(getParentModule())等同mobility*/

            getParentModule()->getDisplayString().updateWith("r=16,red");
            my_setup->setTargetID(getParentModule()->getIndex());     /*更改為車輛號次*/
            am_i_a_tracker = 0;
            positionOftarget = TraCIMobilityAccess().get(getParentModule())->getCurrentPosition();  /*mobility所需要的物件指標跟換色不同*/
        }
    }
    else if (stage == 1) {
        //Initializing members that require initialized other modules goes here
        /*那些需要初始化的其他模塊初始化成員放在這裡。*/

        //manager->getManagedHosts();   /*TraCIScenarioManager.h的類別成員*/
        //prTargetCarModule = manager->getManagedModule("一般車輛[22]");   /*取得特定車輛組件*/

        my_setup->setSendHandoffMessage(false);
        my_setup->setSerial_number(1);
        my_setup->setF_count_of_rsu_reply(0);
        my_setup->setF_unique_report(0);
        my_setup->setLost_count_in_structure(0);
        my_setup->setHandoff_times(1);
        my_setup->setRsu_receive_lost_message(0);
        my_setup->setWe_lost_target(0);
        my_setup->setCrossroad_count(0);


        time_to_report = simTime(); /*設定初始時間*/
        b_at_i = simTime();
        time_to_send_lost_message = simTime();

        /*200台車：初始追蹤者124、目標101*//*300台車：追蹤者170、目標43*//*400台車：追蹤者200、目標5*//*500台車：追蹤者66、目標38*/
        /**
         * 200公尺通訊距離-------------------
         * 200台車：110秒換手失敗、
         *
         *
         * 500台車：80秒換手失敗、通報25次
         *
         * 100公尺通訊距離-------------------
         * 200台車：81秒換手失敗、換手5次、通報23次、RSU收到最新版本是23、RSU回覆144次
         * 300台車：146秒換手失敗、換手8次、通報26次、RSU收到最新版本是8、RSU回覆63次
         * 400台車：100+7秒換手失敗(自定義)、換手4次、通報20次、RSU收到最新版本是2、RSU回覆3次
         * 500台車：180-2秒換手失敗、換手14次、通報44次、RSU收到最親版本是13、RSU回覆170次
         *
         * 200台車：54秒換手失敗、換手6次、通報17次、RSU收到最新版本是16、RSU回復44次 (追蹤者：126、目標：99)
         * 400台車：54秒換手失敗、換手6次、通報17次、RSU收到最新版本是16、RSU回復44次 (追蹤者：0、目標：0)
         *
         * ------------------------------------------------------------------------------------
         *
         * berkeley01
         * 200: 64 112
         * 300: 195 103
         * 82 217
         * 34 43
         * 204 201
         * 12 173
         *
         */

        /*腳本中的車輛資訊*/
        //manager->getManagedModule("一般車輛[71]");
        //manager->moduleName("AAA");
        //manager->moduleName.
        traci->vehicle("Car[71]");

        //manager->getManagedModule("一般車輛[22]")->getDisplayString().updateWith("r=16,red");
        /**/
        /*腳本中的車輛資訊*/

        my_setup->setReplyHandoff(false); /*真假值初始化*/
    }
    else if (stage == 2){
        maxSpeed00 = traci->vehicle("Car[22]").getMaxSpeed();  /*語法測試*/
    }
}

void MyVeinsApp::finish() {
    /*statistics recording goes here    統計記錄在這裡。 */
    /*recordScalar自動記錄到藍色長條圖*/ /*cHistogram記錄到數據不明的彩色長條圖*/
    /*module(RSUExampleScenario.Car[0].appl) AND name("#RSU REPLY")  OR module(RSUExampleScenario.Car[0].appl) AND name("#REPORT COUNT")*/
    /*module(RSUExampleScenario.Car[0].appl) AND name("#RSU RECEIVE")  OR module(RSUExampleScenario.Car[0].appl) AND name("#REPORT COUNT") OR module(RSUExampleScenario.Car[0].appl) AND name("#UNIQUE REPORT OF RSU RECEIVE")*/

    BaseWaveApplLayer::finish();
    recordScalar("#sent_report_count_by_node", report_count_per_node);
    recordScalar("#sent_report_count_in_structure", my_setup->getF_count_of_report());
    recordScalar("#handoff_count_per_node", count_of_handoffmessage);
    recordScalar("#handoff_count_in_structure", my_setup->getF_count_of_handoffmessage());
    recordScalar("#lost_count_by_node", lost_count_by_node);
    recordScalar("#lost_count_in_structure", my_setup->getLost_count_in_structure());
}

void MyVeinsApp::onBSM(BasicSafetyMessage* bsm) {
    //Your application has received a beacon message from another car or RSU
    //您的應用程序已收到來自其他汽車或RSU的信標消息
    //code for handling the message goes here
    //處理消息的代碼在這裡

    if (getParentModule()->getIndex() != target_index)
    {
        if (traci->getDistance(mobility->getCurrentPosition(), positionOftarget, 0) <= MAX_tracking_distance)
        {
            am_i_a_tracker = 1;
            my_setup->insertTrackers_index(getParentModule()->getIndex());
        }
    }
}

void MyVeinsApp::onWSM(WaveShortMessage* wsm) {
    //findHost()->getDisplayString().updateWith("r=16,green");
    //Your application has received a data message from another car or RSU
    //您的應用程序已收到來自其他汽車或RSU的數據消息。
    //code for handling the message goes here, see TraciDemo11p.cc for examples
    //處理消息的代碼在這裡，請參閱TraciDemo11p.cc以獲取示例
    /*還沒有給回覆 收到後再回覆OK 然後再停止換手訊息*/
    /*而且實際上是要離目標夠近才可以回復，所以需要再外加：如果我離目標夠近就回覆的條件*/
    /*收到訊息且距離最近的應該成為Tracker*/
    /*要再呼叫一個 收到訊息的車是否可以成為Tracker的判斷式，並且Tracker只能有一台*/
    /*先比較距離，最近的就是最合適的Tracker*/

    if (getParentModule()->getIndex() != my_setup->getTargetID())
    {
        switch (wsm->getPsid())
        {
            case 2:     /*收到來自Tracker的換手要求*/ /*收到之後要call距離判斷*/
            {
                EV << "現在Send Handoff Message的真假值是 " << my_setup->getSendHandoffMessage() << endl;

                if (distance_with_target() <= wsm->getDistanceKK() && distance_with_target() <= MAX_tracking_distance && my_setup->getSaveDistance() >= MAX_tracking_distance)   /*如果我的距離更近，並且小於最大可追蹤距離*/
                {
                    my_setup->setSendHandoffMessage(false);
                    my_setup->setReplyHandoff(true); /*已得到回覆，真假值調為1*/
                    EV << "(來自case 2)設定Send Handoff Message的真假值為 " << my_setup->getSendHandoffMessage() << endl;
                    WaveShortMessage* replyHandoffMessage = new WaveShortMessage("Reply Handoff");
                    populateWSM(replyHandoffMessage);
                    replyHandoffMessage->setSenderAddress(getParentModule()->getIndex());
                    replyHandoffMessage->setRecipientAddress(wsm->getSenderAddress());
                    replyHandoffMessage->setSenderPos(mobility->getCurrentPosition());     /*curPosition*/
                    replyHandoffMessage->setPsid(3);    /*訊息型態：3，reply*/
                    replyHandoffMessage->setSerial(2);
                    replyHandoffMessage->setUserPriority(6);
                    EV << "回覆換手要求，傳送 " << replyHandoffMessage << endl;
                    replyHandoffMessage->setDistanceKK(distance_with_target());   /*取到全部的距離之後還要做比較*/
                    /*最短的將它set為Tracker*/ /*uniform(a, b)取隨機數*/
                    sendDelayedDown(replyHandoffMessage, uniform(0.01,0.2));
                }

                break;
            }
            case 3:     /*收到reply，指定新的Tracker*/
            /*只需要發出指定訊息一次就好*/   /*直接指定第一個回覆換手要求的 或是 等都到全部的回覆之後再選個距離最短的*/
            /*確定沒有訊息進來之後再發出指定？怎麼達成？*/
            {
                if (getParentModule()->getIndex() == my_setup->getTrackerID() && my_setup->getAlreadysentAssignWSM() == false)    /*如果我是Tracker*//*從Reply裡面選一個最近的或第一個回覆的*/
                /*如果沒有AlreadysentAssignWSM的真假值判斷，可能每收到一次reply就會發一次*/
                {
                    /*必須要先開始暫存回報*/  /*if收到新reply*/
                    /*處理完所有的回報之後，再產生指定訊息*/

                    //findHost()->getDisplayString().updateWith("i=veins/node/car;is=vs");
                    //findHost()->setDisplayString("i=veins/node/car;is=vs");
                    //if (time_to_assign + 3 >= simTime())
                    //else sendDelayedDown(AssignTrackerWSM, simTime() + 2 + uniform(0.01,0.2));

                    my_setup->setSendHandoffMessage(false);
                    EV << "(來自case 3)設定send handoff message的真假值為 " << my_setup->getSendHandoffMessage() << endl;
                    WaveShortMessage* AssignTrackerWSM = new WaveShortMessage("Assign New Tracker");
                    populateWSM(AssignTrackerWSM);
                    AssignTrackerWSM->setSenderAddress(getParentModule()->getIndex());
                    AssignTrackerWSM->setUserPriority(7);
                    AssignTrackerWSM->setSerial(2);
                    AssignTrackerWSM->setPsid(4);   /*訊息類型：4*/
                    AssignTrackerWSM->setNEWTrackerCarID(wsm->getSenderAddress());
                    my_setup->setTrackerID(wsm->getSenderAddress());          /*直接設定新的tracker*/
                    my_setup->setRequest_count(0);
                    EV << "現在のTracker IDは  " << my_setup->getTrackerID() << endl;
                    sendDown(AssignTrackerWSM);
                    my_setup->setHandoff_times(my_setup->getHandoff_times() + 1);
                    EV << "現在的換手次數是  " << my_setup->getHandoff_times() << endl;
                    handoff_times = handoff_times + 1;
                    my_setup->setRequest_count(0);
                    my_setup->setAlreadysentAssignWSM(true);
                    break;
                }
            }
            case 4:
            {
                //my_setup->setTrackerID(wsm->getNEWTrackerCarID());
                my_setup->setSendHandoffMessage(false);
                EV << "(來自case 4)設定send handoff message的真假值為 " << my_setup->getSendHandoffMessage() << endl;
                my_setup->setReplyHandoff(false);
                EV << "ハンドオフした、新たなTrackerは " << my_setup->getTrackerID() << " !" << endl;
                EV << "現在Send Handoff Message的真假值是 " << my_setup->getSendHandoffMessage();
                break;
            }
            case 5: /*如果收到同樣內容要先放棄轉發*/ /*通報訊息*/ /*在時間內收到同樣訊息*/
            {
                if (wsm->getSenderAddress() != getParentModule()->getIndex() && am_i_a_tracker == 0)
                {
                    if (wsm->getForward_count() < 1)
                    {
                        //if (traci->getDistance(mobility->getCurrentPosition(), positionOftarget, 0) <= MAX_tracking_distance) am_i_a_tracker = 1;

                        if (wsm->getMy_direction() == mobility->getAngleRad())
                        {
                            distance_with_sender = (1 - traci->getDistance(mobility->getCurrentPosition(), wsm->getSenderPos(), 0)/100) * delay_time;
                            forward_delay = distance_with_sender;
                            forward_delay = fabs(forward_delay);
                            WaveShortMessage* re_report_wsm = new WaveShortMessage("RE:通報メッセージ");
                            populateWSM(re_report_wsm);
                            re_report_wsm->setTargetPos(wsm->getTargetPos());
                            re_report_wsm->setSenderAddress(wsm->getSenderAddress());
                            re_report_wsm->setTimestamp(wsm->getTimestamp());
                            re_report_wsm->setForward_count(wsm->getForward_count() + 1);
                            re_report_wsm->setWsmData("forward");
                            re_report_wsm->setSerial(2);
                            re_report_wsm->setPsid(5);
                            scheduleAt(simTime() + forward_delay, re_report_wsm);
                        }


                    }
                    if (wsm->getForward_count() >= 1)
                    {
                        EV << "接收到已被轉傳過的訊息，將在柱列中檢查重複事件" << endl;
                        simtime_t the_timestamp_0 = wsm->getTimestamp();
                        for (int event_id = 0; event_id <= cSimulation::getActiveSimulation()->getFES()->getLength() - 1; event_id++)   /*在未來事件集合中尋找目標事件*/
                        {
                            if (cSimulation::getActiveSimulation()->getFES()->get(event_id)->isMessage() == 1)  /*如果某事件屬於訊息事件*/
                            {
                                if (WaveShortMessage* wsm_check = dynamic_cast<WaveShortMessage*>(cSimulation::getActiveSimulation()->getFES()->get(event_id)))   /*將事件轉換為WSM*/
                                {
                                    if (wsm_check->getForward_from() == getParentModule()->getIndex() && wsm_check->getTimestamp() == the_timestamp_0 && wsm_check->getPsid() == 5)    /*如果該事件來自自己波並且時間戳相同*/
                                    {
                                        cSimulation::getActiveSimulation()->getFES()->remove(cSimulation::getActiveSimulation()->getFES()->get(event_id));
                                        event_id = cSimulation::getActiveSimulation()->getFES()->getLength() - 1;
                                        EV << "已在事件柱列中找到自己的訊息" << endl;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            }
            case 10:
            {

            }
            case 11:
            {
                if (getParentModule()->getId() == wsm->getRecipientAddress())
                {
                    my_setup->setRsu_receive_lost_message(1);
                    my_setup->setRequest_count(0);
                    my_setup->setTrackerID(-1);
                    //my_setup->setRequest_count(0);
                }
                break;
            }
            case 20:
            {
                if (traci->getDistance(mobility->getCurrentPosition(), positionOftarget, 0) <= MAX_tracking_distance)
                {
                    my_setup->insertTrackers_index(getParentModule()->getIndex());
                    if (am_i_a_tracker == 0)
                    {
                        my_setup->setF_count_of_handoffmessage(my_setup->getF_count_of_handoffmessage() + 1);
                        count_of_handoffmessage = count_of_handoffmessage + 1;
                    }
                    am_i_a_tracker = 1;
                }
                else
                {
                    wsm->setPsid(21);
                    wsm->setSerial(2);
                    wsm->setWsmData("RE");
                    my_setup->setCrossroad_count(my_setup->getCrossroad_count() + 1);
                    scheduleAt(simTime() + 2, wsm->dup());
                    findHost()->bubble("RE!");
                }
            }
            case 21:
            {
                if (traci->getDistance(mobility->getCurrentPosition(), positionOftarget, 0) <= MAX_tracking_distance)
                {
                    my_setup->insertTrackers_index(getParentModule()->getIndex());
                    if (am_i_a_tracker == 0)
                    {
                        my_setup->setF_count_of_handoffmessage(my_setup->getF_count_of_handoffmessage() + 1);
                        count_of_handoffmessage = count_of_handoffmessage + 1;
                    }
                    am_i_a_tracker = 1;
                }
            }
        }
    }
}

void MyVeinsApp::onWSA(WaveServiceAdvertisment* wsa) {
    //Your application has received a service advertisement from another car or RSU
    //您的應用程序已收到來自其他汽車或RSU的服務宣傳
    //code for handling the message goes here, see TraciDemo11p.cc for examples
    /*處理訊息的程式碼在這裡，請參閱TraciDemo11p.cc以獲取範例*/
    //sendDown(wsa);
    //scheduleAt(simTime()+1, wsa);

    /*以下額外增加*/

}

void MyVeinsApp::handleSelfMsg(cMessage* msg) {
    //BaseWaveApplLayer::handleSelfMsg(msg);
    //this method is for self messages (mostly timers)
    /*這種方法適用於自我消息（主要是定時器）*/
    //it is important to call the BaseWaveApplLayer function for BSM and WSM transmission
    /*調用BaseWaveApplLayer函數進行BSM和WSM傳輸非常重要*/

    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg)) {
        //send this message on the service channel until the counter is 3 or higher.
        //在服務頻道上發送此消息，直到計數器為3或更高
        //this code only runs when channel switching is enabled
        //此代碼僅在啟用通道切換時運行

        //manager = TraCIScenarioManagerAccess().get();
        /*TraCIScenarioManager* manager;於.h檔宣告*/
        TraCICommandInterface* traciInterface = manager->getCommandInterface();

        /*TraCICommandInterface* getCommandInterface於TraCIScenarioManager.h中*/
        //mobility = manager->getManagedModule("一般車輛[22]");
        //mobility->getManagedModule("一般車輛[22]");
        //traciVehicle("一般車輛[22]")
        //distanceCalculate = traci->getDistance(curPosition, positionOftarget, 0);
        //TraCICommandInterface obj000;
        //TraCICommandInterface obj000();
        //Veins::TraCICommandInterface obj003;
        //obj003.getDistance(curPosition, positionOftarget, returnDrivingDistance)
        //TraCICommandInterface::getDistance obj001;
        //distanceCalculate = TraCICommandInterface::getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = TraCICommandInterface::getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = obj000.getDistance(curPosition, positionOftarget, 1);
        //distanceCalculate = MIXIM_API::distance(positionOftarget);

        sendDown(wsm->dup());
        wsm->setSerial(wsm->getSerial() +1);
        if (wsm->getSerial() >= 3) {        /*Serial會影響到要傳送幾次！*/
            stopService();                  /*stop service advertisements*/
            delete(wsm);
        }
        else {
            scheduleAt(simTime(), wsm);
        }
    }
    else {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }
}

void MyVeinsApp::handlePositionUpdate(cObject* obj) {
    BaseWaveApplLayer::handlePositionUpdate(obj);
    //the vehicle has moved. Code that reacts to new positions goes here.
    /*車輛已經移動了。 對新職位做出反應的代碼就在這裡。*/
    //member variables such as currentPosition and currentSpeed are updated in the parent class
    /*DO NOT USE WHILE*/

    tracking_status();
    vehicle_setup(getParentModule()->getIndex());  /*General Car or Tracker Car*/

}

void MyVeinsApp::tracking_status()
{
    if (my_setup->getTrackers_index_empty() == 0) tracking_bool.record(1);
    else tracking_bool.record(0);
}

double MyVeinsApp::distance_with_target()  /*一般車輛用*/
{
    double distance00 = traci->getDistance(mobility->getCurrentPosition(), positionOftarget, 0);
    return distance00;
}

void MyVeinsApp::vehicle_setup(int MyCarID)
{
    if (MyCarID == my_setup->getTargetID())
    {
        positionOftarget = mobility->getCurrentPosition();
    }
    else if (am_i_a_tracker == 1)  /*如果我是Tracker*/
    {
        findHost()->getDisplayString().updateWith("r=16,green");

        send_ReportWSM();

        if (traci->getDistance(mobility->getCurrentPosition(), positionOftarget, 0) > MAX_tracking_distance)    /*目標遺失判斷*/
        {
            findHost()->bubble("HANDOFF!");
            findHost()->setDisplayString("i=veins/node/car;is=vs");
            send_handoffWSM();
            am_i_a_tracker = 0;         /*我已不是tracker*/
            my_setup->removeTrackers_index(getParentModule()->getIndex());
            lost_count_by_node = lost_count_by_node + 1;
            my_setup->setLost_count_in_structure(my_setup->getLost_count_in_structure() + 1);

        }
        else if (mobility->getRoadId().find("_", mobility->getRoadId().length() - 2) != string::npos && mobility->getRoadId().find("1", mobility->getRoadId().length() - 1) != string::npos)
        {
            if (b_at_i + 3 <= simTime())
            {
                send_handoffWSM();
                b_at_i = simTime();
            }
        }

    }
    else
    {
        findHost()->setDisplayString("i=veins/node/car;is=vs");
    }
}

void MyVeinsApp::TargetPositionUpdate(int MyCarID)  /*不使用*/
{
    if (MyCarID == my_setup->getTargetID()) positionOftarget = mobility->getCurrentPosition();
}


void MyVeinsApp::send_ReportWSM()    /*在傳完後設定時間，定時執行試試看*/
{
    WaveShortMessage* report_wsm = new WaveShortMessage("通報メッセージ");
    populateWSM(report_wsm);
    report_wsm->setMy_direction(mobility->getAngleRad());
    report_wsm->setSenderAddress(getParentModule()->getIndex());
    report_wsm->setPsid(5);
    report_wsm->setSerial(2);
    report_wsm->setWsmVersion(my_setup->getSerial_number() + 1);
    report_wsm->setSenderPos(mobility->getCurrentPosition());
    report_wsm->setWsmData("通報メッセージ");
    report_wsm->setTargetPos(positionOftarget);
    report_wsm->setForward_count(0);
    sendDown(report_wsm);
    my_setup->setSerial_number(my_setup->getSerial_number() + 1);
    my_setup->setF_count_of_report(my_setup->getF_count_of_report() + 1);
    report_count_per_node = report_count_per_node + 1;
}

void MyVeinsApp::send_Handoff()   /*失敗的話一直發出遺失訊息，直到RSU回覆*/
{
    WaveShortMessage* handoffRequest = new WaveShortMessage("ハンドオフリクエスト");
    populateWSM(handoffRequest);
    handoffRequest->setSenderAddress(getParentModule()->getIndex());
    handoffRequest->setPsid(2);     /*訊息種類*/
    handoffRequest->setSerial(2);   /*傳送次數*/
    handoffRequest->setSenderPos(mobility->getCurrentPosition());
    handoffRequest->setDistanceKK(my_setup->getSaveDistance());
    handoffRequest->setWsmData("遺失追蹤車輛");
    handoffRequest->setTorF(my_setup->getReplyHandoff());  /*問題：replyHandoff都不變成true*/
    time_to_assign = simTime(); /*傳送ハンドオフ的時間*/
    sendDown(handoffRequest);
}


void MyVeinsApp::send_handoffWSM()      /*NEW*/
{
    WaveShortMessage* handoffRequest = new WaveShortMessage("NEWハンドオフリクエスト");
    populateWSM(handoffRequest);
    handoffRequest->setSenderAddress(getParentModule()->getIndex());
    handoffRequest->setPsid(20);     /*訊息種類*/
    handoffRequest->setSerial(0);   /*傳送3次*/
    handoffRequest->setSenderPos(mobility->getCurrentPosition());
    handoffRequest->setWsmData("遺失追蹤車輛");
    handoffRequest->setTorF(my_setup->getReplyHandoff());  /*問題：replyHandoff都不變成true*/
    scheduleAt(simTime(), handoffRequest);
}
