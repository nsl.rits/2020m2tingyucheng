//
// Copyright (C) 2006-2011 Christoph Sommer <christoph.sommer@uibk.ac.at>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
/**
 * 當汽車停止時間超過10秒時，它會向包含阻塞道路ID的其他汽車發送信息。
 * 然後，接收汽車將通過TraCI觸發重新路由。
 * 當在MAC上啟用SCH和CCH之間的信道切換時，消息將在CCH上的WAVE服務廣告之後在服務信道上發送出去。
 */
//原始範例所使用的應用是這個

#include "veins/modules/application/traci/TraCIDemo11p.h"

Define_Module(TraCIDemo11p);

void TraCIDemo11p::initialize(int stage) {  //好像都會先初始化。雙冒號代表全域函數，前面為class名稱，後面是成員的名稱。
    BaseWaveApplLayer::initialize(stage);   //呼叫類別BaseWaveApplLayer的成員
    if (stage == 0) {
        sentMessage = false; //預設為否定
        lastDroveAt = simTime();
        currentSubscribedServiceId = -1;
    }

    /*以下是額外增加*/

    /*以上是額外增加*/
}

void TraCIDemo11p::onWSA(WaveServiceAdvertisment* wsa) {
    if (currentSubscribedServiceId == -1) {
        mac->changeServiceChannel(wsa->getTargetChannel());
        currentSubscribedServiceId = wsa->getPsid();
        if  (currentOfferedServiceId != wsa->getPsid()) {
            stopService();
            startService((Channels::ChannelNumber) wsa->getTargetChannel(), wsa->getPsid(), "Mirrored Traffic Service");
        }
    }
}

void TraCIDemo11p::onWSM(WaveShortMessage* wsm) {
    findHost()->getDisplayString().updateWith("r=16,green");
    /*updateWith：使用另一個顯示字符串的內容進行更新。 新顯示字符串中的相應元素將覆蓋現有值。*/

    if (mobility->getRoadId()[0] != ':') traciVehicle->changeRoute(wsm->getWsmData(), 9999);
    if (!sentMessage) {
        sentMessage = true;
        //repeat the received traffic update once in 2 seconds plus some random delay
        //在2秒內重複一次接收的流量更新加上一些隨機延遲。
        wsm->setSenderAddress(myId);    /*註記：這裡有ID。它的宣告藏在BaseWaveApplLayer.h*/
        wsm->setSerial(3);
        scheduleAt(simTime() + 2 + uniform(0.01,0.2), wsm->dup());  /*scheduleAt(time, event)代表要在什麼時候產生這個事件或訊息*/
        /*simTime()會取得當前的simulation time，而uniform(a, b)用來取得參數之間的隨機數*/
    }
}

void TraCIDemo11p::handleSelfMsg(cMessage* msg) {
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg)) {
        //send this message on the service channel until the counter is 3 or higher.
        //在服務頻道上發送此消息，直到計數器為3或更高
        //this code only runs when channel switching is enabled
        //此代碼僅在啟用通道切換時運行
        sendDown(wsm->dup());
        wsm->setSerial(wsm->getSerial() +1);
        if (wsm->getSerial() >= 3) {           /*當Serial大於3時，將不再繼續並刪除訊息*/
            //stop service advertisements
            stopService();
            delete(wsm);
        }
        else {                                 /*否則繼續排程*/
            scheduleAt(simTime()+1, wsm);      /*又重新開始schedule(time, message)*/
        }
    }
    else {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }
}

void TraCIDemo11p::handlePositionUpdate(cObject* obj) {
    BaseWaveApplLayer::handlePositionUpdate(obj);

    // stopped for for at least 10s?
    if (mobility->getSpeed() < 1) {
        if (simTime() - lastDroveAt >= 10 && sentMessage == false) {
            findHost()->getDisplayString().updateWith("r=16,red");  /*標註車子*/
            sentMessage = true;

            WaveShortMessage* wsm = new WaveShortMessage();
            populateWSM(wsm);
            wsm->setWsmData(mobility->getRoadId().c_str());

            //host is standing still due to crash
            if (dataOnSch) {
                startService(Channels::SCH2, 42, "Traffic Information Service");
                //started service and server advertising, schedule message to self to send later
                scheduleAt(computeAsynchronousSendingTime(1,type_SCH),wsm);
            }
            else {
                //send right away on CCH, because channel switching is disabled
                sendDown(wsm);
            }
        }
    }
    else {
        lastDroveAt = simTime();
    }
}
