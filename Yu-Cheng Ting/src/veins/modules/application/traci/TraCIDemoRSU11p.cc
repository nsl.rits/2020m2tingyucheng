//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 total_reportle Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "veins/modules/application/traci/TraCIDemoRSU11p.h"    //引入viens自帶的標頭檔

/*標頭檔裡面所引入的函式庫會使RSU發送beacon*/

/*以下為額外引入的標頭檔*/
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
/*以上為額外引入的標頭檔*/

int total_report = 0;
int a1 = 1;
bool lost_the_target = 0;
RSUsystem* rsu_system00 = new RSUsystem;

Define_Module(TraCIDemoRSU11p);



void TraCIDemoRSU11p::handleSelfMsg(cMessage* msg)
{
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg))
    {
        wsm->setSerial(wsm->getSerial() +1);
        if (wsm->getSerial() >= 3) {        /*Serial會影響到要傳送幾次！*/
            //stop service advertisements
            stopService();
            delete(wsm);
        }
        else {
            scheduleAt(simTime() + 10, wsm);
        }
    }
    else
    {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }

}

void TraCIDemoRSU11p::onWSA(WaveServiceAdvertisment* wsa) {
    //if this RSU receives a WSA for service 42, it will tune to the chan
    if (wsa->getPsid() == 42) {                   /*Psid是Provider Service Context提供商服務背景(正在使用的服務的標識符)*/
        mac->changeServiceChannel(wsa->getTargetChannel()); /*獲取mac相關資訊*/
    }
}

void TraCIDemoRSU11p::onWSM(WaveShortMessage* wsm) {    /*應該可以正確使用到wsm變數的引數*/
    //this rsu repeats the received traffic update in 2 seconds plus some random delay
    //這個rsu在2秒內重複接收到的流量更新加上一些隨機延遲。

    /*回復通報訊息：已收到*/
    //sendDelayedDown(wsm->dup(), 2 + uniform(0.01,0.2)); /*原始存在，暫時隱藏。dup()的宣告來自WaveShortMessage_m.h*/
    /*uniform(a, b)用來取a與b之間的隨機數*/

    if (wsm->getPsid() == 20)
    {
        WaveShortMessage* reply = new WaveShortMessage("受け取った！");
        populateWSM(reply);
        reply->setSenderAddress(getParentModule()->getIndex());
        reply->setWsmVersion(wsm->getWsmVersion());
        reply->setSenderAddress(getParentModule()->getIndex());
        reply->setPsid(0);
        reply->setRecipientAddress(wsm->getSenderAddress());
        reply->setWsmData("來自RSU的回覆");
        sendDown(reply);
    }
    else if (wsm->getPsid() == 101)
    {
        lost_the_target = 1;
        sendBeacons = 1;
        EV << "現在lost_the_target的真假值是  " << lost_the_target << endl;
        WaveShortMessage* reply_lost = new WaveShortMessage("Receive Lost Message");
        populateWSM(reply_lost);
        reply_lost->setSenderAddress(getParentModule()->getId());   /*MAC層的位址是my_id*/
        reply_lost->setPsid(11);
        reply_lost->setRecipientAddress(wsm->getSenderAddress());
        reply_lost->setWsmData("收到遺失訊息");
    }

}

void TraCIDemoRSU11p::handlePositionUpdate(cObject* obj)
{
    if (simTime() >= 10)
    {
        WaveShortMessage* reply = new WaveShortMessage("Test");
        populateWSM(reply);
        reply->setSenderAddress(getParentModule()->getIndex());
        reply->setPsid(0);
        reply->setWsmData("來自RSU的回覆");
    }
}

void TraCIDemoRSU11p::send_lost_message()
{

}

void TraCIDemoRSU11p::finish(){
    recordScalar("#RSU COUNT", total_report);
    //recordScalar("#UNIQUE COUNT", unique_report);
}
