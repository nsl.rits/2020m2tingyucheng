//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/modules/application/traci/crossroadAPP.h>
#include "veins/modules/messages/WaveShortMessage_m.h"
#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"
#include <string>
#include <iostream>

using namespace std;

Define_Module(crossroadAPP);

setup_crossroad* c01 = new setup_crossroad;

void crossroadAPP::initialize(int stage)
{
    BaseWaveApplLayer::initialize(stage);

    if (stage == 0)
    {
        EV << "stage = 1 " << endl;
    }
    else if (stage == 1)
    {
        EV << "stage = 2" << endl;
        traci->getJunctionIds();
        mobility->getRoadId();

        //manager->addModule("apple", "applea_type", "iphone", "iphone_11", p1, "564684545_3", 0, 60, manager->VEH_SIGNAL_NONE);
    }
    else if (stage == 3)
    {
        EV << "stage = 3" << endl;
    }
}

void crossroadAPP::finish()
{

}

void crossroadAPP::onBSM(BasicSafetyMessage* bsm)
{

}

void crossroadAPP::handleSelfMsg(cMessage* msg)
{
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg))
    {
        wsm->setSerial(wsm->getSerial() +1);
        if (wsm->getSerial() >= 3) {        /*SerialπeΏvBτI*/
            //stop service advertisements
            stopService();
            delete(wsm);
        }
        else {
            scheduleAt(simTime() + 1, wsm);
        }
    }
    else {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }


}



void crossroadAPP::onWSM(WaveShortMessage* wsm)
{

}

void crossroadAPP::handlePositionUpdate(cObject* obj)
{
    BaseWaveApplLayer::handlePositionUpdate(obj);

    /*traci->getDistance(mobility->getCurrentPosition(), traci->junction("564684545").getPosition(), 0) <= 6 && sent_msg == 0*/
    /*mobility->getRoadId().find("_", mobility->getRoadId().length() - 3) != string::npos && mobility->getRoadId().find("1", mobility->getRoadId().length() - 2) != string::npos*/
    if (mobility->getRoadId().find("_", mobility->getRoadId().length() - 2) != string::npos && mobility->getRoadId().find("1", mobility->getRoadId().length() - 1) != string::npos)
    {
        EV << "Approaching intersection." << endl;


        list_j = traci->getJunctionIds().begin();
        advance(list_j, 10);
        r_id = *list_j;

        EV << "My road ID = " << mobility->getRoadId() << endl;

        EV << "Length = " << mobility->getRoadId().length() << endl;

        WaveShortMessage* none_msg = new WaveShortMessage("nothing");
        populateWSM(none_msg);
        my_road_id = mobility->getRoadId();
        c = my_road_id.data();
        d = r_id.data();
        none_msg->setWsmData(c);
        none_msg->setPsid(20);     /*u§νή*/
        none_msg->setSerial(2);   /*B3*/
        sendDown(none_msg);
        sent_msg = 1;
    }
}
