//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "veins/modules/application/traci/intersectionTest.h"
#include "veins/modules/messages/WaveShortMessage_m.h"
#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"

using namespace std;
using namespace Veins;

Define_Module(intersectionTest);

void intersectionTest::initialize(int stage)
{
    //BaseWaveApplLayer::initialize(stage);

    if (stage == 0)
    {

    }
    else if (stage == 1)
    {

    }
}

void intersectionTest::finish()
{

}

void intersectionTest::onBSM(BasicSafetyMessage* bsm)
{

}

void intersectionTest::onWSM(WaveShortMessage* wsm)
{

}

void intersectionTest::onWSA(WaveServiceAdvertisment* wsa)
{

}

void intersectionTest::handleSelfMsg(cMessage* msg)
{
    if (WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg))
    {
        wsm->setSerial(wsm->getSerial() +1);
        if (wsm->getSerial() >= 3) {        /*SerialπeΏvBτI*/
            //stop service advertisements
            stopService();
            delete(wsm);
        }
        else {
            scheduleAt(simTime() + 1, wsm);
        }
    }
    else {
        BaseWaveApplLayer::handleSelfMsg(msg);
    }


}

void intersectionTest::handlePositionUpdate(cObject* obj)
{
    if (traci->getDistance(mobility->getCurrentPosition(), traci->junction("2784431002").getPosition(), 0) > 1)
    {
        WaveShortMessage* handoffRequest00 = new WaveShortMessage("aaa");
        populateWSM(handoffRequest00);

        handoffRequest00->setPsid(20);     /*u§νή*/
        handoffRequest00->setSerial(2);   /*B3*/

        scheduleAt(simTime(), handoffRequest00);
    }
}




























