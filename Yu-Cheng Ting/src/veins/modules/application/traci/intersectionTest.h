//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MODULES_APPLICATION_TRACI_INTERSECTIONTEST_H_
#define SRC_VEINS_MODULES_APPLICATION_TRACI_INTERSECTIONTEST_H_

#include "omnetpp.h"
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIColor.h"
#include "veins/base/utils/Coord.h"
#include "veins/modules/world/traci/trafficLight/TraCITrafficLightProgram.h"

#include "veins/modules/mac/ieee80211p/Mac1609_4.h"

using namespace omnetpp;
using namespace Veins;

class intersectionTest: public BaseWaveApplLayer {
    public:
        virtual void initialize(int stage);
        virtual void finish();
    private:
        TraCIScenarioManager* manager;
        TraCIConnection* myconnection;

    protected:
        virtual void onBSM(BasicSafetyMessage* bsm);
        virtual void onWSM(WaveShortMessage* wsm);
        virtual void onWSA(WaveServiceAdvertisment* wsa);
        virtual void handleSelfMsg(cMessage* msg);
        virtual void handlePositionUpdate(cObject* obj);
};

#endif /* SRC_VEINS_MODULES_APPLICATION_TRACI_INTERSECTIONTEST_H_ */
