//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MODULES_APPLICATION_TRACI_reportMessage_H_
#define SRC_VEINS_MODULES_APPLICATION_TRACI_reportMessage_H_

#include <veins/modules/application/ieee80211p/BaseWaveApplLayer.h>
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIColor.h"
#include "veins/base/utils/Coord.h"
#include "veins/modules/world/traci/trafficLight/TraCITrafficLightProgram.h"
#include "veins/modules/mac/ieee80211p/Mac1609_4.h"
#include <string>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include "veins/base/utils/Move.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"
#include "veins/modules/mobility/traci/TraCICoord.h"
#include <veins/modules/mobility/traci/TraCICoordinateTransformation.h>

using namespace omnetpp;
using namespace Veins;
using Veins::TraCIMobilityAccess;
using namespace std;

class reportMessage: public BaseWaveApplLayer {
    public:
        virtual void initialize(int stage);
        virtual void finish();

        simtime_t forward_t = 100000;
        simtime_t ready_to_send = 21;
        int wait_t = 18;
        int car_id = 38;
        double distance_s;
        double angle_calculate;
        Coord t_pos;
        const double p = 3.14159;
        bool msg_s = 0;
        bool temp_bool;
        TraCICoord coord_traci;
        eventnumber_t my_msg_event;
        const char *eventnameis;
        cFutureEventSet *event_set;
        queue<WaveShortMessage*> wsm_queue;
        set<WaveShortMessage*> wsm_set;
        set<int> just_for_test;
        cOutVector v_test;


    private:
        TraCIScenarioManager* manager;
        TraCIConnection* myconnection;
        TraCICoordinateTransformation* t_coordinate;
        TraCIConnection* traci_connection;
        TraCICoordinateTransformation* coord_trans;

    protected:
        virtual void onBSM(BasicSafetyMessage* bsm);
        virtual void onWSM(WaveShortMessage* wsm);
        virtual void handleSelfMsg(cMessage* msg);
        virtual void handlePositionUpdate(cObject* obj);
};

class rm_class
{
    protected:
        set<int> id_store;
        simtime_t ttt;

    public:
        int getId_store_empty()
        {
            return this->id_store.empty();
        }
        void setId_sotre(int id_s)
        {
            id_store.insert(id_s);
        }
        void removeId_store(int your_id)
        {
            id_store.erase(your_id);
        }

        simtime_t getTtt() const
        {
            return this->ttt;
        }
        void setTtt(simtime_t ttt)
        {
            this->ttt = ttt;
        }

};


#endif /* SRC_VEINS_MODULES_APPLICATION_TRACI_reportMessage_H_ */
